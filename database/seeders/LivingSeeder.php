<?php

namespace Database\Seeders;
use App\Models\LivingRoom;
use App\Models\Room;
use App\Models\Student;
use Faker\Factory as FakerFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LivingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();

        $dc= DB::table('rooms')->pluck('dorm_code');
        $sd= DB::table('students')->pluck('dorm_code');

        if ( $dc == $sd) {
            $floor = $faker->numberBetween(1, 2);
        } elseif ($room_number >= 101 && $room_number <= 200) {
            $floor = $faker->numberBetween(3, 4);
        } elseif ($room_number >= 201 && $room_number <= 300) {
            $floor = 5;
        }

        for ($i = 1; $i <= 50; $i++) {

                DB::table('living')->insert([
                    'student_number' => $students->student_number,
                    'room_code' => $faker->randomElement($rooms->pluck('room_code')->toArray()),
                    'start_date' => $faker->dateTimeBetween('-2 year', 'now'),
                    'end_date' => $faker->dateTimeBetween('now', '+1 year'),
                ]);
                $livingRoom->empty_capacity = $livingRoom->empty_capacity - 1;

        }
    }
}

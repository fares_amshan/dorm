<?php

namespace Database\Seeders;
use Faker\Factory as FakerFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();
        $dc= DB::table('dorms')->pluck('dorm_code');
        $ssn= DB::table('Persons')->pluck('ssn');
        for ($i = 1; $i <= 50; $i++) {

            DB::table('students')->insert([
            'student_number' => $faker->unique()->numberBetween(1000, 9999),
            'field' => $faker->jobTitle,
            'semester' => $faker->numberBetween(1, 8),
            'status' => $faker->randomElement(['Full-time', 'Part-time']),
            'dorm_code' => $faker->randomElement($dc),
            'ssn' => $faker->unique()->randomElement($ssn),
            ]);
        }
    }
}

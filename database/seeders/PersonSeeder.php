<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();

        for ($i = 1; $i <= 100; $i++) {
            DB::table('Persons')->insert([
            'ssn' => $faker->unique()->numberBetween(1000000000, 9999999999),
            'name' => $faker->firstName,
            'family' => $faker->lastName,
            'sex' => $faker->randomElement(['M', 'F']),
            'address' => $faker->address
            ]);

        }
    }
}

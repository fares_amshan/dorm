<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;

class FacilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();
        $roomc= DB::table('rooms')->pluck('room_code');
        for ($i = 1; $i <= 500; $i++) {
                DB::table('facilities')->insert([
            'facilities_code' => $faker->unique()->numberBetween(100000, 999999),
            'name' => $faker->randomElement(['LG', 'Samsung', 'Sony','Panasonic']),
            'type' => $faker->randomElement(['Monitor', 'Refrigerator', 'Washing Machine','Table','Chair']),
            'room_code' => $faker->randomElement($roomc) ,
            ]);

        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();
        $dc= DB::table('dorms')->pluck('dorm_code');
        for ($i = 1; $i <= 250; $i++) {
            $room_number = $faker->numberBetween(10, 300);
            $floor = 0;

            if ($room_number >= 10 && $room_number <= 100) {
                $floor = $faker->numberBetween(1, 2);
            } elseif ($room_number >= 101 && $room_number <= 200) {
                $floor = $faker->numberBetween(3, 4);
            } elseif ($room_number >= 201 && $room_number <= 300) {
                $floor = 5;
            }

            DB::table('rooms')->insert([
                'room_code' => $faker->unique()->numberBetween(9999, 999999),
                'number' => $room_number,
                'floor' => $floor,
                'dorm_code' => $faker->randomElement($dc),
            ]);
        }
    }
}

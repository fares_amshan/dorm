<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;

class EmployeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();
        $dormc= DB::table('dorms')->pluck('dorm_code');
        $ssn= DB::table('Persons')->pluck('ssn');

        for ($i = 1; $i <= 50; $i++) {
            DB::table('employes')->insert([
            'employe_code' => $faker->unique()->numberBetween(10000, 99999),
            'type' => $faker->randomElement(['Manager', 'Supervisor', 'Security']),
            'salary' => $faker->randomFloat(2982, 4000, 5000),
            'dorm_code' => $faker->randomElement($dormc),
            'ssn' => $faker->unique()->randomElement($ssn),
                ]);

        }
    }
}

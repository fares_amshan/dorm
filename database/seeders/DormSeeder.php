<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;

class DormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();

        for ($i = 1; $i <= 10; $i++) {
            DB::table('dorms')->insert([
            'dorm_code' => $faker->unique()->numberBetween(100, 999),
            'name' => $faker->company,
            'address' => $faker->address,
            'type' => $faker->randomElement(['Male', 'Female', 'Mixed']),
            'room_count' => $faker->numberBetween(30, 100),
            'capacity' => $faker->numberBetween(500, 1000),
            ]);

        }
    }
}

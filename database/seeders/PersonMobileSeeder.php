<?php

namespace Database\Seeders;

use Faker\Provider\ar_EG\Person;
use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Facades\DB;
class PersonMobileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = FakerFactory::create();
        $ssn= DB::table('Persons')->pluck('ssn');

        for ($i = 1; $i <= 50; $i++) {
            DB::table('person_mobile')->insert([
            'phone1' => $faker->unique()->phoneNumber,
            'phone2' => $faker->unique()->phoneNumber,
            'ssn' => $faker->unique()->randomElement($ssn),
            ]);
        }
    }
}

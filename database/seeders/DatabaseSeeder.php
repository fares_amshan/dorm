<?php

namespace Database\Seeders;

use App\Models\LivingRoom;
use Illuminate\Database\Seeder;
use Database\Seeders\PersonSeeder;

class DatabaseSeeder extends Seeder

{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
           // PersonSeeder::class,
            //PersonMobileSeeder::class,
            //DormSeeder::class,
           //EmployeSeeder::class,
            //RoomSeeder::class,
            //StudentSeeder::class,
            //FacilitiesSeeder::class,
            //LivingRoomSeeder::class,
            LivingSeeder::class,

            // وهكذا لكل Seeder الخاص بك
        ]);
    }
}

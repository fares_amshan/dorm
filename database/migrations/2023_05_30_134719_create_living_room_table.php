<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLivingRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('living_rooms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('room_code');
            $table->integer('capacity');
            $table->integer('empty_capacity');
            $table->foreign('room_code')->references('room_code')->on('rooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('living_room');
    }
}

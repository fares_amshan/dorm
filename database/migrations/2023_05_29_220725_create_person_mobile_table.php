<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonMobileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_mobile', function (Blueprint $table) {
            $table->id();
            $table->string('phone1', 255);
            $table->string('phone2', 255);
            $table->unsignedBigInteger('ssn');
            $table->foreign('ssn')->references('ssn')->on('persons');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_mobile');
    }
}

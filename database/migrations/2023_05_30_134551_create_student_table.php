<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->unsignedBigInteger('student_number')->unique()->primary();
            $table->string('field', 255);
            $table->integer('semester');
            $table->string('status', 255);
            $table->unsignedBigInteger('dorm_code');


            $table->foreign('dorm_code')
                ->references('dorm_code')
                ->on('dorms');

            $table->unsignedBigInteger('ssn');
            $table->foreign('ssn')
                ->references('ssn')
                ->on('persons');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}

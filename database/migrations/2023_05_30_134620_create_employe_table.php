<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->id();
            $table->integer('employe_code')->unique();
            $table->string('type', 255);
            $table->decimal('salary', 10, 2);
            $table->unsignedBigInteger('dorm_code');
            $table->unsignedBigInteger('ssn');
            $table->foreign('dorm_code')->references('dorm_code')->on('dorms');
            $table->foreign('ssn')->references('ssn')->on('persons');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employe');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLivingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('living', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_number');
            $table->unsignedBigInteger('room_code');
            $table->date('start_date');
            $table->date('end_date');
            $table->foreign('student_number')->references('student_number')->on('students');
            $table->foreign('room_code')->references('room_code')->on('rooms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('living');
    }
}

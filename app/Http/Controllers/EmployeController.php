<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use App\Models\Person;
use App\Models\Employe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeController extends Controller
{

    public function index()
    {
        //$students = DB::table('students')->get();
        $persons= Person::orderBy('ssn')->paginate(50);
        $employe = Employe::orderBy('dorm_code')->paginate(50);
        $dorms = Dorm::get('dorm_code');

        return view('pages.forms.employe', compact('employe','persons','dorms'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'ssn' => 'required|unique:persons',
            'name' => 'required',
            'family' => 'required',
            'sex' => 'required',
            'address' => 'required',
            'employe_code' => 'required|unique:employes',
            'type' => 'required',
            'salary' => 'required',
           // 'ssn' => 'required',
            'dorm_code' => 'required',
        ]);
        $now = now();
        DB::table('persons')->insert([
            'ssn' => $request->input('ssn'),
            'name' => $request->input('name'),
            'family' => $request->input('family'),
            'sex' => $request->input('sex'),
            'address' => $request->input('address'),
            'created_at' => $now,

        ]);
        DB::table('employes')->insert([
            //'id'=>$request->insertGetId(),
            'ssn' => $request->input('ssn'),
            'employe_code' => $request->input('employe_code'),
            'type' => $request->input('type'),
            'salary' => $request->input('salary'),
            'dorm_code' => $request->input('dorm_code'),
            'created_at' => $now,

        ]);


        $request->session()->flash('success',  'Employe added successfully!');
        return redirect()->route('employes.store');
    }

    public function showAllEmployes()
    {
        $employes = DB::table('employes')
            ->join('persons', 'persons.ssn', '=', 'employes.ssn')
            ->select('employes.*', 'persons.*')
            ->paginate(1000);

        return view('pages.tables.employes', [
            'employes' => $employes,
        ]);
    }

    public function profile($ssn)
    {
        $dorms = Dorm::orderBy('dorm_code')->paginate(50);
        $employes = Employe::where('ssn', $ssn)->first();
        $persons = Person::where('ssn', $ssn)->first();

        return view('pages.forms.editemploye', compact('employes','persons','ssn','dorms'));
    }

    public function update(Request $request, $ssn)
    {

        $validatedData = $request->validate([
            'ssn' => 'required',
            'name' => 'required',
            'family' => 'required',
            'sex' => 'required',
            'address' => 'required',
            'salary' => 'required',
            'employe_code' => 'required',
            'type' => 'required',
            'dorm_code' => 'required',
        ]);

        DB::table('persons')
            ->where('ssn', $ssn)
            ->update([
                'name' => $request->input('name'),
                'family' => $request->input('family'),
                'address' => $request->input('address'),
                'ssn' => $request->input('ssn'),
                'sex' => $request->input('sex'),
            ]);

        DB::table('employes')
            ->where('ssn', $ssn)
            ->update(
                [
                    'employe_code' => $request->input('employe_code'),
                    'salary' => $request->input('salary'),
                    'type' => $request->input('type'),
                    'dorm_code' => $request->input('dorm_code'),
                ]
            );


        return redirect()->route('employes.view');
    }
}

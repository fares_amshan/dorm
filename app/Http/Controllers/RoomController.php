<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    public function index()
    {
        //$students = DB::table('students')->get();
        //$persons= Person::orderBy('ssn')->paginate(50);
        //$employe = Student::orderBy('dorm_code')->paginate(50);
        $dorms = Dorm::get('dorm_code');

        return view('pages.forms.room', compact('dorms'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'room_code' => 'required|unique:rooms',
            'dorm_code' => 'required',
            'count' => 'required',
            'floor' => 'required',
        ]);
        $now = now();

        $floor = $request->input('floor');
        $count = $request->input('count');
        $room_code=$request->input('room_code');
        if ($floor = 1) {
            $number = 10;
        } elseif ($floor = 2) {
            $number = 20;
        } elseif ($floor = 3) {
            $number = 30;
        }elseif ($floor = 4) {
            $number = 40;
        }elseif ($floor = 5) {
            $number = 50;
        }elseif ($floor = 6) {
            $number = 60;
        }

        for ($i = 1; $i <= $count; $i++) {
        DB::table('rooms')->insert([
            'room_code' => $room_code++ ,
            'dorm_code' => $request->input('dorm_code'),
            'number' => $number++,
            'floor' => $floor,
            'created_at' => $now,

        ]);}


        $request->session()->flash('success',  'Dorm added successfully!');
        return redirect()->route('rooms.store');
    }
}

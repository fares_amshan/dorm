<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use App\Models\Employe;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DormController extends Controller
{
    public function index()
    {
        //$students = DB::table('students')->get();
        //$persons= Person::orderBy('ssn')->paginate(50);
        //$employe = Student::orderBy('dorm_code')->paginate(50);
        $dorms = Dorm::get('dorm_code');

        return view('pages.forms.dorm', compact('dorms'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'dorm_code' => 'required|unique:dorms',
            'name' => 'required',
            'address' => 'required',
            'type' => 'required',
            'room_count' => 'required',
             'capacity' => 'required',
        ]);
        $now = now();
        DB::table('dorms')->insert([
            'dorm_code' => $request->input('dorm_code'),
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'type' => $request->input('type'),
            'room_count' => $request->input('room_count'),
            'capacity' => $request->input('capacity'),
            'created_at' => $now,

        ]);


        $request->session()->flash('success',  'Dorm added successfully!');
        return redirect()->route('dorms.store');
    }

    public function showAllDorms()
    {
        $dorms = DB::table('dorms')
            ->select('dorms.*')
            ->paginate(1000);

        return view('pages.tables.dorms', [
            'dorms' => $dorms,
        ]);
    }

    public function profile($dorm_code)
    {
        //$dorms = Dorm::orderBy('dorm_code')->paginate(50);
        $dorms = Dorm::where('dorm_code', $dorm_code)->first();

        return view('pages.forms.editdorm', compact('dorms','dorm_code'));
    }

    public function update(Request $request, $dorm_code)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'type' => 'required',
            'address' => 'required',
            'dorm_code' => 'required',
            'capacity' => 'required',
            'room_count' => 'required',
        ]);

        DB::table('dorms')
            ->where('dorm_code', $dorm_code)
            ->update([
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'type' => $request->input('type'),
                'room_count' => $request->input('room_count'),
                'capacity' => $request->input('capacity'),
            ]);
        $request->session()->flash('success', 'Student added successfully!');
        return redirect()->route('dorms.view');
    }


    public function rooms()
    {
        $dorms = Dorm::orderBy('dorm_code')->paginate(50);
        return view('pages.tables.rooms', compact('dorms'));
    }

    public function showroomms(Request $request, $dorm_code)
    {
        $rooms = DB::table('rooms')
            ->join('dorms', 'dorms.dorm_code', '=', 'rooms.dorm_code')
            ->select('rooms.*', 'dorms.*')
            ->where('dorms.dorm_code', '=', $dorm_code)
            ->paginate(1000);

        return view('pages.tables.drooms', [
            'rooms' => $rooms,
            'dorm_code'=>$dorm_code
        ]);
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
       {
           $studentsCount = DB::table('students')->count();
           $employesCount = DB::table('employes')->count();
           $roomsCount = DB::table('rooms')->count();
           $dormsCount = DB::table('dorms')->count();
           return view('index', compact('studentsCount','employesCount','roomsCount','dormsCount'));
       }
}

<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function showDorms()
    {
        $dorms = Dorm::orderBy('dorm_code')->paginate(50);
        return view('pages.forms.student', compact('dorms'));
    }
}

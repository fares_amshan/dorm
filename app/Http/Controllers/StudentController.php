<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use App\Models\Person;
use App\Models\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function index()
    {
        //$students = DB::table('students')->get();
        $persons= Person::orderBy('ssn')->paginate(50);
        $students = Student::orderBy('dorm_code')->paginate(50);
        $dorms = Dorm::get('dorm_code');

        return view('pages.forms.student', compact('students','persons','dorms'));
    }
    public function showAllStudents()
    {
        $students = DB::table('students')
            ->join('persons', 'persons.ssn', '=', 'students.ssn')
            ->select('students.*', 'persons.*')
            ->paginate(1000);

        return view('pages.tables.students', [
            'students' => $students,
        ]);
    }

    public function view()
    {
        //$students = DB::table('students')->get();
        $persons= Person::orderBy('ssn')->paginate(50);
        $students = Student::orderBy('dorm_code')->paginate(50);
        $dorms = Dorm::get('dorm_code');

        return view('pages.tables.students', compact('students','persons','dorms'));
    }

    public function showDorms()
    {
        $dorms = Dorm::orderBy('dorm_code')->paginate(50);
        return view('pages.forms.student', compact('dorms'));
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'ssn' => 'required|unique:persons',
            'name' => 'required',
            'family' => 'required',
            'sex' => 'required',
            'address' => 'required',
            'field' => 'required',
            'semester' => 'required',
            'student_number' => 'required|unique:students',
            'status' => 'required',
            'dorm_code' => 'required',
        ]);
        $now = now();

        DB::table('persons')->insert([
            'ssn' => $request->input('ssn'),
            'name' => $request->input('name'),
            'family' => $request->input('family'),
            'sex' => $request->input('sex'),
            'address' => $request->input('address'),
            'created_at' => $now,

        ]);
        DB::table('students')->insert([
            'field' => $request->input('field'),
            'semester' => $request->input('semester'),
            'student_number' => $request->input('student_number'),
            'ssn' => $request->input('ssn'),
            'status' => $request->input('status'),
            'dorm_code' => $request->input('dorm_code'),
            'created_at' => $now,

        ]);


        $request->session()->flash('success', 'Student added successfully!');
        return redirect()->route('students.store');
    }

    public function show($id)
    {
        $student = DB::table('students')->where('id', $id)->first();

        return view('students.show', ['student' => $student]);
    }

    public function edit($id)
    {
        $student = DB::table('students')->where('id', $id)->first();

        return view('students.edit', ['student' => $student]);
    }

    public function profile($ssn)
    {
        $dorms = Dorm::orderBy('dorm_code')->paginate(50);
        $students = Student::where('ssn', $ssn)->first();
        $persons = Person::where('ssn', $ssn)->first();

        //return view('pages.forms.editestudent')->with('student', $student)->with('person', $person);
        return view('pages.forms.editestudent', compact('students','persons','ssn','dorms'));
    }

    public function update(Request $request, $ssn)
    {

        $validatedData = $request->validate([
            'ssn' => 'required',
            'name' => 'required',
            'family' => 'required',
            'sex' => 'required',
            'address' => 'required',
            'field' => 'required',
            'semester' => 'required',
            'student_number' => 'required',
            'status' => 'required',
            'dorm_code' => 'required',
        ]);

        DB::table('persons')
            ->where('ssn', $ssn)
            ->update([
                'name' => $request->input('name'),
                'family' => $request->input('family'),
                'address' => $request->input('address'),
                'ssn' => $request->input('ssn'),
                'sex' => $request->input('sex'),
            ]);

        DB::table('students')
            ->where('ssn', $ssn)
            ->update(
                [
                    'student_number' => $request->input('student_number'),
                    'field' => $request->input('field'),
                    'semester' => $request->input('semester'),
                    'status' => $request->input('status'),
                    'dorm_code' => $request->input('dorm_code'),
                ]
            );


        return redirect()->route('students.view');
    }

    public function destroy($id)
    {
        DB::table('students')->where('id', $id)->delete();

        return redirect()->route('students.index');
    }
    public function stimdorm()
    {
        $results = DB::table('dorms')
            ->join('students', 'dorms.dorm_code', '=', 'students.dorm_code')
            ->join('persons', 'students.ssn', '=', 'persons.ssn')
            ->select('persons.name', 'persons.family', 'persons.address', 'dorms.dorm_code','dorms.name')
            ->get();

        return view('index', compact('results'));
    }

    public function showFemale()
    {
        $students = DB::table('students')
            ->join('persons', 'persons.ssn', '=', 'students.ssn')
            ->select('students.*', 'persons.*')->where('Persons.sex', '=', 'F')
            ->paginate(1000);

        return view('pages.tables.female', [
            'students' => $students,
        ]);
    }

    public function showmale()
    {
        $students = DB::table('students')
            ->join('persons', 'persons.ssn', '=', 'students.ssn')
            ->select('students.*', 'persons.*')->where('sex', '=', 'M')
            ->paginate(1000);

        return view('pages.tables.male', [
            'students' => $students,
        ]);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LivingRoom extends Model
{
    use HasFactory;
    protected $table = 'living_rooms';
    protected $fillable = [
        'empty_capacity',
    ];
}

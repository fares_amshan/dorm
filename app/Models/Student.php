<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table = 'students';
    protected $primaryKey = 'student_number';
    protected $fillable = [
        'student_number',
        'semester',
        'field',
        'status',
        'dorm_code',
    ];

    public function persons()
    {
        return $this->belongsTo(Person::class, 'ssn');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $table = 'persons';

    protected $fillable = [
        'ssn',
        'sex',
        'name',
        'family',
        'address',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function fullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}

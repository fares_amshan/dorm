@include("header")
<body class="">
<div class="wrapper">
    <div class="sidebar">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
      -->
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="javascript:void(0)" class="simple-text logo-mini">

                </a>
                <a href="javascript:void(0)" class="simple-text logo-normal">
                    Menu
                </a>
            </div>
            <ul class="nav">
                <li>
                    <a href="">
                        <i class="tim-icons icon-chart-pie-36"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active ">
                    <a href="">
                        <i class="tim-icons icon-single-02"></i>
                        <p>Students</p>
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="tim-icons icon-puzzle-10"></i>
                        <p>Professors</p>
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="tim-icons icon-puzzle-10"></i>
                        <p>Students Trash</p>
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="tim-icons icon-puzzle-10"></i>
                        <p>Professors Trash</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @include("navbar")
<div class="main-panel">
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="card-header">
                    <h4 class="card-title">Students</h4>
                </div>
                <div class="card-body">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                        <div class="table-responsive">
                            <table class="table tablesorter " id="">
                                <thead class=" text-primary">
                                <tr>
                                    <th> </th>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">name</th>
                                    <th class="text-center">family name</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">
                                        Student Number
                                    </th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>{{$i=0}}
                                @foreach ($results as $result)
                                    <tr>
                                        <td>

                                        </td>

                                        <td class="text-center">{{ $i++ }}</td>
                                        <td class="text-center">{{ $result->dorm_code }}</td>
                                        <td class="text-center">{{ $result->name }}</td>
                                        <td class="text-center">{{ $result->family }}</td>
                                        <td class="text-center">{{ $result->address }}</td>
                                        <td class="text-center">{{ $result->name }}</td>
                                        <td class="text-center">
                                            @endforeach
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                </div>
            </div>
            </div>
            @include("footer")
        </div>



@include("mode")
@include("jsfiles")

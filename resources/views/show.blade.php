<!DOCTYPE html>
<html>
<head>
    <title>{{ $student->name }}</title>
</head>
<body>
<h1>{{ $student->name }}</h1>

<p><strong>Email:</strong> {{ $student->email }}</p>
<p><strong>Phone:</strong> {{ $student->phone }}</p>
<!-- إضافة المزيد من الحقول الأخرى هنا -->

<a href="{{ route('students.edit', $student->id) }}">Edit</a>
<form action="{{ route('students.destroy', $student->id) }}"method="POST">
    @csrf
    @method('DELETE')
    <button type="submit">Delete</button>
</form>
</body>
</html>

<div class="main-panel">
    <div class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="title">Edit Student "{{$student->name}}"</h5>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.student.update', $student)}}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-md-5 pr-md-1">
                                    <div class="form-group">
                                        <label>Number</label>
                                        <input type="text" name="number" class="form-control" value="{{ $student->number }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-md-1">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Number" value="{{$student->name }}">
                                    </div>
                                </div>
                                <div class="col-md-4 px-md-1">
                                    <div class="form-group">
                                        <label>Father Name</label>
                                        <input type="text" name="fathername" class="form-control"  placeholder="Name" value="{{ $student->father_name }}">
                                    </div>
                                </div>

                                <div class="col-md-4 pl-md-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Family Name</label>
                                        <input type="text" name="familyname" class="form-control" value="{{ $student->family_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-md-1">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" class="form-control" name="phone" value="{{ $student->phone}}" placeholder="{{ $student->phone}}">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-md-1">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control" value="{{ $student->email}}" placeholder="{{ $student->email }}" name="email">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-fill btn-primary">Save</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@include("header")
@include("navbar")
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" id="success-alert">
                      {{ session('success') }}
                  </div>
              @endif

            <div class="page-header">

              <h3 class="page-title"> Dorm </h3>

            </div>
              <form method="POST" action="{{ route('dorms.store') }}">
                  @csrf
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add New Dorm</h4>
                    <form class="form-sample">
                      <p class="card-description"> Dorm info </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{ old('name') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Room Count</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="room_count"value="{{ old('room_count') }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address" value="{{ old('address') }}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Type</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="type" value="Male" checked> Male </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="type" value="Female"> Female </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Capacity</label>
                            <div class="col-sm-9">
                              <input type="number" class="form-control" name="capacity" value="{{ old('capacity') }}" />
                            </div>
                          </div>
                        </div>
                          <div class="col-md-6">
                              <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Dorm Code</label>
                                  <div class="col-sm-9">
                                      <input type="number" class="form-control" name="dorm_code" value="{{ old('dorm_code') }}"/></div>
                              </div>
                          </div>

                      </div>


                        <div class="template-demo">
                            <button type="submit" class="btn btn-primary btn-icon-text">
                                <i class="mdi mdi-file-check btn-icon-prepend"></i> Submit </button>
                        </div>
                    </form>  </div>
                          </div>
                        </div>
                      </form>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
<script>
    // Hide the success message after 5 seconds
    setTimeout(function() {
        $('#success-alert').fadeOut('slow');
    }, 4000);
</script>
@include("footer")


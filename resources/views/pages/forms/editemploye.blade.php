@include("header")
@include("navbar")
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" id="success-alert">
                      {{ session('success') }}
                  </div>
              @endif

            <div class="page-header">

              <h3 class="page-title"> Edit Employe  </h3>

            </div>
                  <form method="POST" action="{{ route('employes.edit', ['ssn' => $employes->ssn]) }}">
                  @csrf
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Edit Employe: {{$persons->name}} </h4>
                    <form class="form-sample">
                      <p class="card-description"> Employe info </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{$persons->name}}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Family</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="family" value="{{$persons->family}}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address" value="{{$persons->address}}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Sex</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label" >
                                                <input type="radio" class="form-check-input" name="sex" value="M" {{ $persons->sex == 'M' ? 'checked' : '' }}> Male </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="sex" value="F" {{ $persons->sex == 'F' ? 'checked' : '' }}> Female </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Employe Code</label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" name="employe_code" value="{{$employes->employe_code}}" readonly tabindex="-1" />
                            </div>
                          </div>
                        </div>

                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Salary</label>
                              <div class="col-sm-9">
                                  <input type="number" class="form-control" name="salary" value="{{$employes->salary}}"/></div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">SSN</label>
                            <div class="col-sm-9">
                              <input class="form-control" placeholder="" name="ssn" value="{{$employes->ssn}}"  readonly tabindex="-1"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">

                          <div class="col-md-6">
                              <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Dorm Code</label>
                                  <div class="col-sm-9">
                                      <select class="form-control" name="dorm_code">
                                          @foreach ($dorms as $dorm)
                                              <option value="{{ $dorm->dorm_code }}" name="dorm_code"
                                                  {{ old('dorm_code', $employes->dorm_code == $dorm->dorm_code ? 'selected' : '') }}>
                                              {{ $dorm->dorm_code }}
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                          </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Type</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="type" id="membershipRadios1" value="Supervisor" {{ $employes->type == 'Supervisor' ? 'checked' : '' }}> Supervisor </label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input" name="type" id="membershipRadios2" value="Security" {{ $employes->type == 'Security' ? 'checked' : '' }}> Security </label>
                              </div>
                            </div>
                              <div class="col-sm-5">
                                  <div class="form-check">
                                      <label class="form-check-label">
                                          <input type="radio" class="form-check-input" name="type" id="membershipRadios2" value="Manager" {{ $employes->type == 'Manager' ? 'checked' : '' }}> Manager </label>
                                  </div>
                              </div></div></div>



                          </div>
                        </div>



                      </div>


                        <div class="template-demo">
                            <button type="submit" class="btn btn-primary btn-icon-text">
                                <i class="mdi mdi-file-check btn-icon-prepend"></i> Submit </button>
                        </div>
                    </form>  </div>
                          </div>
                        </div>
                      </form>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
<script>
    // Hide the success message after 5 seconds
    setTimeout(function() {
        $('#success-alert').fadeOut('slow');
    }, 4000);
</script>
@include("footer")


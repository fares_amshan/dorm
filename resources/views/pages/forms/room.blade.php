@include("header")
@include("navbar")
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
              @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" id="success-alert">
                      {{ session('success') }}
                  </div>
              @endif

            <div class="page-header">

              <h3 class="page-title"> Room </h3>

            </div>
              <form method="POST" action="{{ route('rooms.store') }}">
                  @csrf
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add New Room</h4>
                    <form class="form-sample">
                      <p class="card-description"> Room info </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Rooms Code</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="room_code" value="{{ old('room_code') }}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Floor</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="floor"value="{{ old('floor') }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Count</label>
                            <div class="col-sm-9">
                              <input type="number" class="form-control" placeholder="" name="count" value="{{ old('count') }}"/>
                            </div>
                          </div>
                        </div>
                          <div class="col-md-6">
                              <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Dorm Code</label>
                                  <div class="col-sm-9">
                                      <select class="form-control" name="dorm_code">
                                          @foreach ($dorms as $dorm)
                                              <option name="dorm_code" value="{{ $dorm->dorm_code }}">{{ $dorm->dorm_code }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                          </div>
                      </div>


                        <div class="template-demo">
                            <button type="submit" class="btn btn-primary btn-icon-text">
                                <i class="mdi mdi-file-check btn-icon-prepend"></i> Submit </button>
                        </div>
                    </form>  </div>
                          </div>
                        </div>
                      </form>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
<script>
    // Hide the success message after 5 seconds
    setTimeout(function() {
        $('#success-alert').fadeOut('slow');
    }, 4000);
</script>
@include("footer")


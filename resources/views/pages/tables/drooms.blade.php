@include("header")
@include("navbar")<body>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Dorms </h3>
            </div>
            <div class="row">

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Dorms table</h4>
                    <div class="table-responsive">
                        <table class="table table-dark">
                            <thead>
                            <tr>
                                <th> # </th>
                                <th> Room Code </th>
                                <th> Number </th>
                                <th> Floor </th>
                                <th> Dorm Code </th>
                            </tr>
                            </thead>
                            <tbody {{$i=1}}>

                            @foreach ($rooms as $room)
                                <tr>
                                    <td> {{$i++}} </td>
                                    <td> {{ $room->room_code }} </td>
                                    <td> {{ $room->number }} </td>
                                    <td> {{ $room->floor }} </td>
                                    <td> {{ $room->dorm_code }} </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
 @include("footer")

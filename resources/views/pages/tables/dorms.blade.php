@include("header")
@include("navbar")<body>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Dorms </h3>
            </div>
            <div class="row">

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Dorms table</h4>
                    <div class="table-responsive">
                      <table class="table table-dark">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th> Name </th>
                              <th> Dorm Code </th>
                              <th> Room Count </th>
                              <th> Capacity </th>
                              <th> Address </th>
                              <th> Type </th>
                              <th>  </th>
                          </tr>
                        </thead>
                        <tbody {{$i=1}}>

                        @foreach ($dorms as $dorm)
                          <tr>
                            <td> {{$i++}} </td>
                            <td> {{ $dorm->name }} </td>
                              <td> {{ $dorm->dorm_code }} </td>
                              <td> {{ $dorm->room_count }} </td>
                              <td> {{ $dorm->capacity }} </td>
                              <td> {{ $dorm->address }} </td>
                              <td> {{ $dorm->type }} </td>
                              <td> <div class="template-demo mt-2"><a href="{{ route('dorms.profile', $dorm->dorm_code) }}">
                                  <button type="button" class="btn btn-outline-secondary btn-icon-text"> Edit <i class="mdi mdi-file-check btn-icon-append"></i>
                                  </button></a>
                              </div></td>
                          </tr>
                        @endforeach

                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
 @include("footer")

@include("header")
@include("navbar")<body>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Employes </h3>
            </div>
            <div class="row">

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Employes table</h4>
                    <div class="table-responsive">
                      <table class="table table-dark">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th> Name </th>
                              <th> Family </th>
                            <th> Sex </th>
                              <th> SSN </th>
                            <th> Employe Number </th>
                              <th> Dorm Code </th>
                              <th> Type </th>
                              <th> Salary </th>
                              <th>  </th>
                          </tr>
                        </thead>
                        <tbody {{$i=1}}>

                        @foreach ($employes as $employe)
                          <tr>
                            <td> {{$i++}} </td>
                            <td> {{ $employe->name }} </td>
                              <td> {{ $employe->family }} </td>
                              <td> {{ $employe->sex }} </td>
                              <td> {{ $employe->ssn }} </td>
                            <td> {{ $employe->employe_code }} </td>
                              <td> {{ $employe->dorm_code }} </td>
                              <td> {{ $employe->type }} </td>
                              <td> {{ $employe->salary }} </td>
                              <td> <div class="template-demo mt-2"><a href="{{ route('employes.profile', $employe->ssn) }}">
                                  <button type="button" class="btn btn-outline-secondary btn-icon-text"> Edit <i class="mdi mdi-file-check btn-icon-append"></i>
                                  </button></a>
                              </div></td>
                          </tr>
                        @endforeach

                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
 @include("footer")

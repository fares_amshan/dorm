@include("header")
@include("navbar")<body>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Dorms </h3>
            </div>
            <div class="row">

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Dorms table</h4>
                    <div class="table-responsive">

                            <div class="col-md-4">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Dorm Code</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="dorm_code" onchange="updateButton(this)">
                                            @foreach ($dorms as $dorm)
                                                <option value="{{ $dorm->dorm_code }}" name="dorm_code">
                                                {{ $dorm->dorm_code }}
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <div class="template-demo mt-2"><a href="{{ route('dorms.rooms', $dorm->dorm_code) }}" id="show-rooms-button" onclick="showRooms()">
                                            <button type="button" class="btn btn-outline-secondary btn-icon-text"> Show Rooms <i class="mdi mdi-file-check btn-icon-append"></i>
                                            </button></a>
                                    </div>
                                </div>
                            </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

            <script>
                function updateButton(select) {
                    var dormCode = select.value;
                    var button = document.getElementById('show-rooms-button');
                    button.href = "{{ route('dorms.rooms', ':dormCode') }}".replace(':dormCode', dormCode);
                }

                function showRooms() {
                    var button = document.getElementById('show-rooms-button');
                    if (button.href != "#") {
                        window.location.href = button.href;
                    }
                }
            </script>
 @include("footer")

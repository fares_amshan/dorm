@include("header")
@include("navbar")<body>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Female Students </h3>
            </div>
            <div class="row">

              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Female Students table</h4>
                    <div class="table-responsive">
                      <table class="table table-dark">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th> Name </th>
                              <th> Family </th>
                            <th> Sex </th>
                              <th> Address </th>
                              <th> SSN </th>
                            <th> Student Number </th>
                              <th> Dorm Code </th>
                              <th>  </th>
                          </tr>
                        </thead>
                        <tbody {{$i=1}}>

                        @foreach ($students as $student)
                          <tr>
                            <td> {{$i++}} </td>
                            <td> {{ $student->name }} </td>
                              <td> {{ $student->family }} </td>
                              <td> {{ $student->sex }} </td>
                              <td> {{ $student->address }} </td>
                              <td> {{ $student->ssn }} </td>
                            <td> {{ $student->student_number }} </td>
                              <td> {{ $student->dorm_code }} </td>
                              <td> <div class="template-demo mt-2"><a href="{{ route('students.profile', $student->ssn) }}">
                                  <button type="button" class="btn btn-outline-secondary btn-icon-text"> Edit <i class="mdi mdi-file-check btn-icon-append"></i>
                                  </button></a>
                              </div></td>
                          </tr>
                        @endforeach

                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
 @include("footer")

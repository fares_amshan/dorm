
@include("header")

<body class="">
  <div class="wrapper">
    <div class="sidebar">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="javascript:void(0)" class="simple-text logo-mini">

          </a>
          <a href="javascript:void(0)" class="simple-text logo-normal">
            Menu
          </a>
        </div>
        <ul class="nav">
          <li>
            <a href="{{Route('student.dashboard')}}">
              <i class="tim-icons icon-chart-pie-36"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="{{Route('student.courses')}}">
              <i class="tim-icons icon-single-02"></i>
              <p>Courses</p>
            </a>
          </li>
            <li>
                <a href="{{Route('student.myCourses')}}">
                    <i class="tim-icons icon-single-02"></i>
                    <p>My Courses</p>
                </a>
            </li>
          <li class="active ">
            <a href="{{Route('student.profile')}}">
              <i class="tim-icons icon-align-center"></i>
              <p>Profile</p>
            </a>
          </li>
        </ul>
      </div>
    </div>


    <div class="main-panel">
      @include("navbar")

      <div class="content">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Student Profile</h5>
              </div>
              <div class="card-body">
                  <div class="card-body">
                      @if(Session::has('message'))
                          <div class="alert alert-success">
                              {{Session::get('message')}}
                          </div>
                      @endif
                      <form>
                          <div class="row">
                          <div class="col-md-5 pr-md-1">
                           <div class="form-group" style="color: #cd23b2">
                           <label>Student Number : </label>
                        {{ Auth::user()->number }}
                                  </div>
                              </div></div>
                          <div class="row">
                              <div class="col-md-4 pr-md-1">
                                  <div class="form-group" style="color: #cd23b2">
                          <label>Student Name :</label>
                            {{ Auth::user()->name }}
                                  </div>
                              </div>
                              <div class="col-md-4 px-md-1">
                                  <div class="form-group" style="color: #cd23b2">
                                      <label>Father Name :</label>
                                      {{ Auth::user()->father_name }}
                                  </div>
                              </div>

                           <div class="col-md-4 pl-md-1">
                             <div class="form-group" style="color: #cd23b2">
                              <label for="exampleInputEmail1">Last Name :</label>
                                   {{ Auth::user()->family_name }}
                                  </div>
                          </div>
                          </div>
                          <div class="row">
                       <div class="col-md-6 pr-md-1">
                           <div class="form-group" style="color: #cd23b2">
                              <label>Phone Number :</label>
                                 {{ Auth::user()->phone}}
                             </div>
                      </div>
                        <div class="col-md-6 pl-md-1">
                         <div class="form-group" style="color: #cd23b2">
                              <label>Email Address :</label>
                      {{ Auth::user()->email }}
                 </div>
               </div>
            </div>
              </form>
                 <div class="card-footer">
                     <a href="./profile/edit"><button type="submit" class="btn btn-fill btn-primary">Edit Profile</button></a>
            </div>
              </div>
            </div>
          </div>

        </div>
      </div>
       @include("footer")
    </div>
  </div>

 @include("mode")

  @include("jsfiles")
  </div>

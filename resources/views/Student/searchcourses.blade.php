@include("header")

<body class="">
    <div class="wrapper">
        <div class="sidebar">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="javascript:void(0)" class="simple-text logo-mini">

                    </a>
                    <a href="javascript:void(0)" class="simple-text logo-normal">
                        Menu
                    </a>
                </div>
                <ul class="nav">
                    <li>
                        <a href="{{Route('student.dashboard')}}">
                            <i class="tim-icons icon-chart-pie-36"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="active ">
                        <a href="{{Route('student.courses')}}">
                            <i class="tim-icons icon-single-02"></i>
                            <p>Courses</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{Route('student.myCourses')}}">
                            <i class="tim-icons icon-single-02"></i>
                            <p>My Courses</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{Route('student.profile')}}">
                            <i class="tim-icons icon-align-center"></i>
                            <p>Profile</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            @include("navbar")
            <div class="content">
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="card ">
                            <div class="card-header">
                                <h4 class="card-title">Courses</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="col-md-5">
                                    <form action="{{Route('search.courses')}}" METHOD="get">

                                            <div class="input-group">

                                                <input type="search" name="search" class="form-control" required="required" placeholder="Search">
                                                <span class="input-group-prepend">
                                                    <button type="submit" class=" btn-primary" ><a href="{{ route('search.courses') }}"></a>Search</button>
                                                 </span>
                                            </div>
                                    </form>
                                    </div>
                                    <table class="table tablesorter" id="">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th class="text-center">
                                                    ID
                                                </th>
                                                <th class="text-center">
                                                    Course name
                                                </th>
                                                <th class="text-center">
                                                    Code
                                                </th>
                                                <th class="text-center">
                                                    Professor
                                                </th>
                                                <th class="text-center">
                                                    Units
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($courses as $course)
                                            @if($course->status)
                                            <tr class="text-center">
                                                <td>{{$course->id}}</td>
                                                <td>{{$course->name}}</td>
                                                <td>{{$course->code}}</td>
                                                <td><a href="{{Route('professor.info',['id'=>$course->professor_id])}}">{{$course->professor->name}}</a></td>
                                                <td>{{$course->units}}</td>
                                                <td>
                                                    <form METHOD="Post" action="{{Route('student.courses.select')}}">
                                                        @csrf
                                                        <input type="hidden" name="course_id" value="{{$course->id}}">
                                                        <input type="submit" value="Select" class="btn btn-success">

                                                    </form></td>
                                            </tr>
                                            @endif
                                        @empty
                                                <tr class="text-center">
                                                    <td colspan="6">notfound</td>
                                                </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                                <div style="text-align: center;margin-left: 42%;">{{$courses->links()}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include("footer")
        </div>
@include("mode")

@include("jsfiles")
    </div>

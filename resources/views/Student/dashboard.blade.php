@include('header')

<body class="">
  <div class="wrapper">
    <div class="sidebar">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="javascript:void(0)" class="simple-text logo-mini">

          </a>
          <a href="javascript:void(0)" class="simple-text logo-normal">
            Menu
          </a>
        </div>
        <ul class="nav">
          <li class="active ">
            <a href="{{Route('student.dashboard')}}">
              <i class="tim-icons icon-chart-pie-36"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="{{Route('student.courses')}}">
              <i class="tim-icons icon-single-02"></i>
              <p>Courses</p>
            </a>
          </li>
            <li>
                <a href="{{Route('student.myCourses')}}">
                    <i class="tim-icons icon-single-02"></i>
                    <p>My Courses</p>
                </a>
            </li>
          <li>
            <a href="{{Route('student.profile')}}">
              <i class="tim-icons icon-align-center"></i>
              <p>Profile</p>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">


      @include("navbar")


      <div class="content">
        <center>
          <div class="col-md-4">
            <div class="card card-user">
              <div class="card-body">
                <p class="card-text">
                  <div class="author">
                    <div class="block block-one"></div>
                    <div class="block block-two"></div>
                    <div class="block block-three"></div>
                    <div class="block block-four"></div>
                    <a href="javascript:void(0)">
                      <img class="avatar" src="{{asset('assets/img/anime3.png')}}" alt="...">
                      <h5 class="title"></h5>
                    </a>
                    <p class="description">
                      Welcome {{ Auth::user()->name }} {{ Auth::user()->family_name }}!
                    </p>
                  </div>
                </p>
                <div class="card-description">
                  This university system will allow you to manage your courses
                </div>
              </div>
              <div class="card-footer">
                <div class="button-container">
                  <button href="javascript:void(0)" class="btn btn-icon btn-round btn-facebook">
                    <i class="fab fa-facebook"></i>
                  </button>
                  <button href="javascript:void(0)" class="btn btn-icon btn-round btn-twitter">
                    <i class="fab fa-twitter"></i>
                  </button>
                  <button href="javascript:void(0)" class="btn btn-icon btn-round btn-google">
                    <i class="fab fa-google-plus"></i>
                  </button>
                </div>
              </div>
            </div>
        </center>
      </div>
      </div>
      @include("footer")

    </div>
  </div>


  @include("mode")

  @include("jsfiles")

@include("header")

<body class="">
    <div class="wrapper">
        <div class="sidebar">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="javascript:void(0)" class="simple-text logo-mini">

                    </a>
                    <a href="javascript:void(0)" class="simple-text logo-normal">
                        Menu
                    </a>
                </div>
                <ul class="nav">
                    <li>
                        <a href="{{Route('student.dashboard')}}">
                            <i class="tim-icons icon-chart-pie-36"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{Route('student.courses')}}">
                            <i class="tim-icons icon-single-02"></i>
                            <p>Courses</p>
                        </a>
                    </li>
                    <li class="active ">
                        <a href="{{Route('student.myCourses')}}">
                            <i class="tim-icons icon-single-02"></i>
                            <p>My Courses</p>
                        </a>
                    </li>
                    <li>
                        <a href="{{Route('student.profile')}}">
                            <i class="tim-icons icon-align-center"></i>
                            <p>Profile</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="main-panel">
            @include("navbar")
            <div class="content">
                <div class="row">
                    <div class="col-lg-10 col-md-10">
                        <div class="card ">
                            <div class="card-header">
                                <h4 class="card-title">Courses</h4>
                            </div>
                            <div class="card-body">
                                @if(Session::has('message'))
                                    <div class="alert alert-success">
                                        {{Session::get('message')}}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table tablesorter" id="">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th class="text-center">
                                                    ID
                                                </th>
                                                <th class="text-center">
                                                    Course name
                                                </th>
                                                <th class="text-center">
                                                    Code
                                                </th>
                                                <th class="text-center">
                                                    Professor
                                                </th>
                                                <th class="text-center">
                                                    Units
                                                </th>

                                            </tr>
                                        </thead>
                                        <tbody>

                                                @foreach($student->courses as $course)

                                                    <tr class="text-center">
                                                <td>{{$course->id}}</td>
                                                <td>{{$course->name}}</td>
                                                <td>{{$course->code}}</td>
                                                <td>{{$course->professor->name}}</td>
                                                <td>{{$course->units}}</td>
                                                 <td>
                                                    <a href="{{Route('student.courses.delete',['id'=>$course->id])}}" name="delete" type="button" class="btn btn-danger">Delete</a>
                                                 </td>

                                            </tr>
                                            @endforeach
                                                <tr class="text-center">
                                                    <td colspan="6">You have {{$unit}} units</td>
                                                </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include("footer")
        </div>
@include("mode")

@include("jsfiles")
    </div>

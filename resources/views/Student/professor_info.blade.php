
@include("header")

<div class="">
  <div class="wrapper">
    <div class="sidebar">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"
    -->
      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="javascript:void(0)" class="simple-text logo-mini">

          </a>
          <a href="javascript:void(0)" class="simple-text logo-normal">
            Menu
          </a>
        </div>
        <ul class="nav">
          <li>
            <a href="{{Route('student.dashboard')}}">
              <i class="tim-icons icon-chart-pie-36"></i>
              <p>Dashboard</p>
            </a>
          </li>
            <li class="active ">
                <a href="{{Route('student.courses')}}">
                    <i class="tim-icons icon-align-center"></i>
                    <p>Courses</p>
                </a>
            </li>
            <li>
                <a href="{{Route('student.myCourses')}}">
                    <i class="tim-icons icon-single-02"></i>
                    <p>My Courses</p>
                </a>
            </li>
          <li>
            <a href="{{Route('student.profile')}}">
              <i class="tim-icons icon-single-02"></i>
              <p>Profile</p>
            </a>
          </li>

        </ul>
      </div>
    </div>


    <div class="main-panel">
      @include("navbar")

      <div class="content">
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Professor Info</h5>
              </div>
              <div class="card-body">
                  <form action="{{ route('student.courses') }}" >
                      @csrf
                      <div class="row">
                          <div class="col-md-4 pr-md-1">
                              <div class="form-group" style="color: #cd23b2">
                                  <label>Name :</label>
                                  {{ $info->name }}
                              </div>
                          </div>
                          <div class="col-md-4 px-md-1">
                              <div class="form-group" style="color: #cd23b2">
                                  <label>Office :</label>
                                  {{ $info->office }}
                              </div>
                          </div>

                          <div class="col-md-4 pl-md-1">
                              <div class="form-group" style="color: #cd23b2">
                                  <label for="exampleInputEmail1">Last Name :</label>
                                  {{ $info->family_name }}
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-6 pr-md-1">
                              <div class="form-group" style="color: #cd23b2">
                                  <label>Phone :</label>
                                {{ $info->phone}}
                              </div>
                          </div>
                          <div class="col-md-6 pl-md-1">
                              <div class="form-group" style="color: #cd23b2">
                                  <label>Email :</label>
                                  {{ $info->email }}
                              </div>
                          </div>
                      </div>
                      <div class="card-footer">
                          <button type="submit" class="btn btn-fill btn-primary"><a href="{{ route('student.courses') }}"></a>Back</button>
                      </div>
                  </form>
              </div>
            </div>
              </div>
            </div>
          </div>

        </div>
      </div>
       @include("footer")
    </div>

 @include("mode")

  @include("jsfiles")



<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');
Route::get('/students', 'App\Http\Controllers\StudentController@index')->name('students');
Route::get('/allst', 'App\Http\Controllers\StudentController@stimdorm');
Route::get('/students/create', 'App\Http\Controllers\StudentController@create')->name('create');

Route::post('/students', 'App\Http\Controllers\StudentController@store')->name('students.store');
Route::get('/viewstudents', 'App\Http\Controllers\StudentController@showAllStudents')->name('students.view');
Route::get('/viewfemales', 'App\Http\Controllers\StudentController@showFemale')->name('students.femalesview');
Route::get('/viewmales', 'App\Http\Controllers\StudentController@showmale')->name('students.malesview');
Route::get('s{ssn}', 'App\Http\Controllers\StudentController@profile')->name('students.profile');
Route::POST('s{ssn}/edit', 'App\Http\Controllers\StudentController@update')->name('students.edit');

Route::get('/employes', 'App\Http\Controllers\EmployeController@index')->name('employes');
Route::post('/employes', 'App\Http\Controllers\EmployeController@store')->name('employes.store');
Route::get('/viewemployes', 'App\Http\Controllers\EmployeController@showAllEmployes')->name('employes.view');
Route::get('e{ssn}', 'App\Http\Controllers\EmployeController@profile')->name('employes.profile');
Route::POST('e{ssn}/edit', 'App\Http\Controllers\EmployeController@update')->name('employes.edit');

Route::get('/dorms', 'App\Http\Controllers\DormController@index')->name('dorms');
Route::post('/dorms', 'App\Http\Controllers\DormController@store')->name('dorms.store');
Route::get('/viewedorms', 'App\Http\Controllers\DormController@showAllDorms')->name('dorms.view');
Route::get('d{dorm_code}', 'App\Http\Controllers\DormController@profile')->name('dorms.profile');
Route::POST('d{dorm_code}/edit', 'App\Http\Controllers\DormController@update')->name('dorms.edit');
Route::get('/chooseroom', 'App\Http\Controllers\DormController@rooms')->name('dorms.sroom');
Route::get('r{dorm_code}', 'App\Http\Controllers\DormController@showroomms')->name('dorms.rooms');

Route::get('/rooms', 'App\Http\Controllers\RoomController@index')->name('rooms');
Route::post('/rooms', 'App\Http\Controllers\RoomController@store')->name('rooms.store');
